import { Model } from "data-base-model/dist/model";
import { ControllerSpring } from "./backend/spring/contoller/controller-spring";
import { RepositorySpring } from "./backend/spring/repository/repository-spring";
import { ModelSpring } from "./backend/spring/models/model-spring";
import { BuildSpring } from "./backend/spring/configuraciones/BuildSpring";
import { RunSpring } from "./backend/spring/configuraciones/RunSpring";
import { PomSpring } from "./backend/spring/configuraciones/PomSpring";
import { PropertiesSpring } from "./backend/spring/configuraciones/PropertiesSpring";
import { AppMainSpring } from "./backend/spring/configuraciones/AppMainSpring";

class Archivo{
    constructor(
        public name: string,
        public content: string
    ){}
}

function getTablaNombreUpperCase(model: Model){        
    let nombre = model.getName().trim();
    nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
    return nombre;
}

function pakagenameToRuta(pakagename:string) {
    return 'src/main/java/'+pakagename.replace(/\./g,'/');
}

export function createBackend(modelos: Model[], pakagename:string, appName:string) {
    let out = [];
    for (let modelo of modelos) {
        const controller = new ControllerSpring(modelo, pakagename);        
        out.push(
            new Archivo(
                pakagenameToRuta(pakagename)+'/controller/'+getTablaNombreUpperCase(modelo)+'Controller.java',
                controller.code
            )
        );

        const repo = new RepositorySpring(modelo, pakagename);
        out.push(
            new Archivo(
                pakagenameToRuta(pakagename)+'/repository/'+getTablaNombreUpperCase(modelo)+'Repository.java',
                repo.code
            )
        );
        
        const modeloSprin = new ModelSpring(modelo, pakagename);
        out.push(
            new Archivo(
                pakagenameToRuta(pakagename)+'/modelo/'+getTablaNombreUpperCase(modelo)+'.java',
                modeloSprin.code
            )
        );
    }
    const build = new BuildSpring();
    const run = new RunSpring(appName);
    const pom = new PomSpring(pakagename, appName);
    const properties = new PropertiesSpring();
    const appMain = new AppMainSpring(pakagename);
    out.push(new Archivo('pom.xml', pom.code));
    out.push(new Archivo('run.bat', run.code));
    out.push(new Archivo('build.bat', build.code));
    out.push(new Archivo('src/main/resources/application.properties', properties.code));
    out.push(new Archivo( pakagenameToRuta(pakagename)+'/Application.java', appMain.code));

    return out;

}