export class PropertiesSpring{
    constructor() {        
    }

    get code() {
        return `
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=admin
spring.datasource.password=

server.port = 12345

spring.jpa.hibernate.ddl-auto=create-drop
spring.datasource.initialization-mode=always
        `;
    }
}