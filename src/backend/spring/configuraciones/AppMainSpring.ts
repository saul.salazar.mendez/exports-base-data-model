export class AppMainSpring {
    constructor(private packagename:string) {}

    get code() {
        return `
package ${this.packagename};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}        
`
    }
}