export class RunSpring {
    constructor(private appName:string) {
    }

    get code(){
        return `java -jar target/${this.appName}-1.0-SNAPSHOT.jar`;
    }
}