import { BaseSpring} from "./base-spring";
import { DATA_TYPES } from "data-base-model/dist/data-type";
import { Atribute } from "data-base-model/dist/atributo";

export class NumberSpring extends BaseSpring {
    constructor(item: Atribute, nombre: string) {
        super(item, nombre, DATA_TYPES.NUMBER);
    }

    public getDeclaracion(): string {
        return `
        private Long ${this.getNombre()};
        `;
    }

    public getMetodosFunciones() {
        return `
    /**
     * @return Long return the ${this.getNombre()}
     */
    public Long get${this.getNombreUpperCase()}() {
        return ${this.getNombre()};
    }

    /**
     * @param ${this.getNombre()} the ${this.getNombre()} to set
     */
    public void set${this.getNombreUpperCase()}(Long ${this.getNombre()}) {
        this.${this.getNombre()} = ${this.getNombre()};
    }
        `;
    }

    public getHeaders(): string{
        return ``;
    }
}