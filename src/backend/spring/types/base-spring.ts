import { Atribute } from "data-base-model/dist/atributo";
import { DATA_TYPES } from "data-base-model/dist/data-type";
import { RULE_TYPE } from "data-base-model/dist/rule";


export class BaseSpring {
     /**
     * @return nombre: return the string
     */
    public getNombre(): string {
        return this.nombre;
    }

    /**
     * @param string the string to set
     */
    public setNombre(nombre: string) {
        this.nombre = nombre;
    }

    /**
     * @return type: return the string
     */
    public getType() {
        return this.type;
    }

    /**
     * @param string the string to set
     */
    public setType(type: string) {
        this.type = type;
    }

    public getDeclaracion() {
        return '';
    };

    public getMetodosFunciones() {
        return '';
    }

    public getHeaders() {
        return '';
    }

    public getNombreUpperCase(){
        let nombre = this.nombre.trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    public getItem() {
        return this.item;
    };

    public getCodeRules() {
        const rules = this.item.getRules;
        let code = '';
        for (let rule of rules) {
            switch(rule.type) {
                case RULE_TYPE.NOT_NULL: {
                    code += `    @NotEmpty\n`;
                }
            }
        }
        return code;
    }

    public getHeadersCodeRules() {
        const rules = this.item.getRules;
        let code = '';
        for (let rule of rules) {
            switch(rule.type) {
                case RULE_TYPE.NOT_NULL: {
                    code += `import javax.validation.constraints.NotEmpty;\n`
                }
            }
        }
        return code;
    }

    constructor(
        private item: Atribute,
        private nombre: string,
        private type: string
    ){

    }
}