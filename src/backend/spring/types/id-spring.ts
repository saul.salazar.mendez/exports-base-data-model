import { BaseSpring} from "./base-spring";
import { Atribute } from "data-base-model/dist/atributo";
import { DATA_TYPES } from "data-base-model/dist/data-type";

export class IdSpring extends BaseSpring {
    constructor(item: Atribute, nombre: string) {
        super(item, nombre, DATA_TYPES.ID);
    }

    public getDeclaracion(): string {
        return `
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private long ${this.getNombre()};
        `;
    }

    public getMetodosFunciones() {
        return `
    /**
     * @return long return the ${this.getNombre()}
     */
    public long get${this.getNombreUpperCase()}() {
        return ${this.getNombre()};
    }

    /**
     * @param ${this.getNombre()} the ${this.getNombre()} to set
     */
    public void set${this.getNombreUpperCase()}(long ${this.getNombre()}) {
        this.${this.getNombre()} = ${this.getNombre()};
    }
        `;
    }

    public getHeaders(): string{
        return `import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;`;
    }
}