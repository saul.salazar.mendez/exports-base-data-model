import { BaseSpring} from "./base-spring";
import { Atribute } from "data-base-model/dist/atributo";
import { DATA_TYPES, ForeingKeyType } from "data-base-model/dist/data-type";

export class ForeingSpring extends BaseSpring {
    constructor(item: Atribute, nombre: string) {
        super(item, nombre, DATA_TYPES.FOREIGN_KEY);
    }

    public getUpperCase(cad:string){
        let nombre = cad.trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    public getLowerCase(cad:string){
        let nombre = cad.trim();
        nombre = nombre.replace(nombre[0], nombre[0].toLowerCase());
        return nombre;
    }

    public getNombreTablaForanea() {
        const forekeing = this.getItem().getType as ForeingKeyType ;
        return this.getUpperCase(forekeing.getTableName);
    }

    public getNombreVariableTablaForanea() {
        const forekeing = this.getItem().getType as ForeingKeyType ;
        return this.getLowerCase(forekeing.getTableName);
    }

    public getDeclaracion(): string {
        const forekeing = this.getItem().getType as ForeingKeyType ;
        return `        
        @ManyToOne
        @JoinColumn(name = "${forekeing.getName.trim()}")
        private ${this.getNombreTablaForanea()} ${this.getNombreVariableTablaForanea()};
        `;
    }

    public getMetodosFunciones() {
        return `
    /**
     * @return  ${this.getNombreTablaForanea()} return the ${this.getNombre()}
     */
    public  ${this.getNombreTablaForanea()} get${this.getNombreTablaForanea()}() {
        return ${this.getNombreVariableTablaForanea()};
    }

    /**
     * @param ${this.getNombre()} the ${this.getNombre()} to set
     */
    public void set${this.getNombreTablaForanea()}( ${this.getNombreTablaForanea()} ${this.getNombreVariableTablaForanea()}) {
        this.${this.getNombreVariableTablaForanea()} = ${this.getNombreVariableTablaForanea()};
    }
        `;
    }

    public getHeaders(): string{
        return `import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;`;
    }
}