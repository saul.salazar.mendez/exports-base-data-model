import { BaseSpring} from "./base-spring";
import { DATA_TYPES } from "data-base-model/dist/data-type";
import { Atribute } from "data-base-model/dist/atributo";

export class StringSpring extends BaseSpring {
    constructor(item: Atribute, nombre: string) {
        super(item, nombre, DATA_TYPES.STRING);
    }

    public getDeclaracion(): string {
        return `
        private String ${this.getNombre()};
        `;
    }

    public getMetodosFunciones() {
        return `
    /**
     * @return String return the ${this.getNombre()}
     */
    public String get${this.getNombreUpperCase()}() {
        return ${this.getNombre()};
    }

    /**
     * @param ${this.getNombre()} the ${this.getNombre()} to set
     */
    public void set${this.getNombreUpperCase()}(String ${this.getNombre()}) {
        this.${this.getNombre()} = ${this.getNombre()};
    }
        `;
    }

    public getHeaders(): string{
        return ``;
    }
}