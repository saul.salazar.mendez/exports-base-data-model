import { Model } from "data-base-model/dist/model";
import { Atribute } from "data-base-model/dist/atributo";
import { DATA_TYPES } from "data-base-model/dist/data-type";
import { StringSpring } from "../types/string-spring";
import { IdSpring } from "../types/id-spring";
import { NumberSpring } from "../types/number-spring";
import { ForeingSpring } from "../types/foreingkey-spring";

export class ModelSpring {
    constructor(public model: Model, public packageName: string) {
    }

    addTrasforms(atributo: Atribute, transforms: any[]) {
        if (atributo.getType.getName == DATA_TYPES.ID) { 
            transforms.push(new IdSpring(atributo, atributo.getName));
        } 
        if (atributo.getType.getName == DATA_TYPES.STRING) { 
            transforms.push(new StringSpring(atributo, atributo.getName));
        }
        if (atributo.getType.getName == DATA_TYPES.NUMBER) { 
            transforms.push(new NumberSpring(atributo, atributo.getName));
        }
        if (atributo.getType.getName == DATA_TYPES.FOREIGN_KEY) { 
            transforms.push(new ForeingSpring(atributo, atributo.getName));
        }        
    }

    getTrasforms() {
        let transforms: any[] = [];
        for (const item of this.model.getAtributos()) {
            this.addTrasforms(item, transforms);
        }
        return transforms;
    }

    public getNombreUpperCase(){
        let nombre = this.model.getName().trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    eliminaRepetidos(texto: string) {
        let array = texto.split('\n');
        let conjunto = new Set<string>();
        for (const item of array) {
            conjunto.add(item);
        }
        let out = '';
        for (const item of conjunto) {
            out += item+'\n';
        }
        return out;
    }

    get code() {
        let code = `package ${this.packageName}.modelo;\nimport javax.persistence.Entity;\n`;
        const transforms = this.getTrasforms();
        let imports = '';
        for (const item of transforms) {
            imports += item.getHeaders();
        }
        code += this.eliminaRepetidos(imports);
        code += `@Entity
public class ${this.getNombreUpperCase()} {`;
        for (const item of transforms) {
            code += item.getDeclaracion();
        }
        for (const item of transforms) {
            code += item.getMetodosFunciones();
        }
        code += `\n}`;
        return code;
    }
}