import { Model } from "data-base-model/dist/model";
import { Atribute } from "data-base-model/dist/atributo";
import { DATA_TYPES, ForeingKeyType } from "data-base-model/dist/data-type";

export class ControllerSpring {
    private nombreTabla: string;
    constructor(public model: Model, public packageName: string) {
        this.nombreTabla = model.getName().trim().toLocaleLowerCase();
    }
    public getTablaNombreUpperCase(){        
        let nombre = this.model.getName().trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    public getTablaNombreLowerCase(){        
        let nombre = this.model.getName().trim();
        nombre = nombre.replace(nombre[0], nombre[0].toLocaleLowerCase());
        return nombre;
    }

    public getNombreUpperCase(nombreAtributo: string){
        let nombre = nombreAtributo.trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    public getAtributoSet(atributo: Atribute) {
        if (atributo.getType.getName != DATA_TYPES.FOREIGN_KEY ) {
            return '            original.set'+
                this.getNombreUpperCase(atributo.getName)+'('+
                `${this.getTablaNombreLowerCase()}.get${this.getNombreUpperCase(atributo.getName)}()`
                +');\n';
        } else {
            const forekeing = atributo.getType as ForeingKeyType ;
            let tipo = forekeing.getTableName;
            tipo = tipo.replace(tipo[0], tipo[0].toUpperCase());
            let variable = forekeing.getTableName;
            variable = variable.replace(variable[0], variable[0].toUpperCase());
            return '            original.set'+
                tipo+'('+`${this.getTablaNombreLowerCase()}.get${tipo}()`+');\n';
        }
    }

    public igualaEntidades() {
        let code = '\n';
        for (const atributo of this.model.getAtributos()) {
            code += this.getAtributoSet(atributo);
        }
        return code;
    }

    get code() {
        let code = `package ${this.packageName}.controller;

import com.fasterxml.jackson.databind.JsonNode;
import ${this.packageName}.modelo.${this.getTablaNombreUpperCase()};
import ${this.packageName}.repository.${this.getTablaNombreUpperCase()}Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

@RestController
public class ${this.getTablaNombreUpperCase()}Controller {

    @Autowired
    private ${this.getTablaNombreUpperCase()}Repository ${this.getTablaNombreUpperCase()}Repository;

    Logger logger = LogManager.getLogger(${this.getTablaNombreUpperCase()}Controller.class);

    @RequestMapping(value = "/${this.nombreTabla}", method = RequestMethod.GET)
    public ResponseEntity<List<${this.getTablaNombreUpperCase()}>> list() {
        Iterable<${this.getTablaNombreUpperCase()}> ${this.getTablaNombreLowerCase()}s = ${this.getTablaNombreUpperCase()}Repository.findAll();
        List<${this.getTablaNombreUpperCase()}> list = new ArrayList<${this.getTablaNombreUpperCase()}>();
        ${this.getTablaNombreLowerCase()}s.forEach(list::add);
        return new ResponseEntity<List<${this.getTablaNombreUpperCase()}>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/${this.nombreTabla}", method = RequestMethod.POST)
    public ${this.getTablaNombreUpperCase()} add${this.getTablaNombreUpperCase()}(@RequestBody ${this.getTablaNombreUpperCase()} ${this.getTablaNombreLowerCase()}){
        ${this.getTablaNombreUpperCase()}  new${this.getTablaNombreUpperCase()} = ${this.getTablaNombreUpperCase()}Repository.save(${this.getTablaNombreLowerCase()});
        return new${this.getTablaNombreUpperCase()};
    }

    @RequestMapping(value = "/${this.nombreTabla}/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<${this.getTablaNombreUpperCase()}>> get${this.getTablaNombreUpperCase()}(
        @PathVariable("id") Long id) {
        Optional<${this.getTablaNombreUpperCase()}> ${this.getTablaNombreUpperCase()} = ${this.getTablaNombreUpperCase()}Repository.findById(id);        
        return new ResponseEntity <Optional<${this.getTablaNombreUpperCase()}>>(${this.getTablaNombreUpperCase()}, HttpStatus.OK);
    }

    @RequestMapping(value="/${this.nombreTabla}/{id}", method = RequestMethod.PUT)
    public ResponseEntity<${this.getTablaNombreUpperCase()}> update(@PathVariable("id") long id,
                                            @RequestBody ${this.getTablaNombreUpperCase()} ${this.getTablaNombreLowerCase()}){
        {
            Optional<${this.getTablaNombreUpperCase()}> find = ${this.getTablaNombreUpperCase()}Repository.findById(id);
            if (!find.isPresent()) {
                ${this.getTablaNombreUpperCase()} nolo = null;
                return new ResponseEntity<${this.getTablaNombreUpperCase()}>(nolo, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            ${this.getTablaNombreUpperCase()} original = find.get();
            ${this.igualaEntidades()}
            ${this.getTablaNombreUpperCase()} update = ${this.getTablaNombreUpperCase()}Repository.save(original);
            return new ResponseEntity<${this.getTablaNombreUpperCase()}>(update, HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "/${this.nombreTabla}", method = RequestMethod.DELETE)
    public Boolean delete${this.getTablaNombreUpperCase()}(@RequestBody JsonNode json){
        if (json.get("id") == null) {
            return false;
        }
        Long id = json.get("id").asLong();
        try {
            ${this.getTablaNombreUpperCase()}Repository.deleteById(id);    
        } catch (Exception e) {
            
        }
        
        return true;
    }
}`;
        return code;
    }
}