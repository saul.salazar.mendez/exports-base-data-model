import { Model } from "data-base-model/dist/model";

export class RepositorySpring {
    constructor(public model: Model, public packageName: string) {
    }
    public getTablaNombreUpperCase(){
        let nombre = this.model.getName().trim();
        nombre = nombre.replace(nombre[0], nombre[0].toUpperCase());
        return nombre;
    }

    get code() {
        let code = `package ${this.packageName}.repository;

import ${this.packageName}.modelo.${this.getTablaNombreUpperCase()};
import org.springframework.data.repository.CrudRepository;

public interface ${this.getTablaNombreUpperCase()}Repository extends CrudRepository<${this.getTablaNombreUpperCase()}, Long> {        
}`;
        return code;
    }
}