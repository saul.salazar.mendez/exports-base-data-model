import { Model } from "data-base-model/dist/model";
import { Atribute } from "data-base-model/dist/atributo";
import { TextType, ForeingKeyType, IntegerType } from "data-base-model/dist/data-type";
import { ModelSpring } from "../spring/models/model-spring";
import { ControllerSpring } from "../spring/contoller/controller-spring";


let db = [];

let persona =  new Model('Persona');
persona.createId();
persona.addAtributo(new Atribute('nombre', new TextType(50), []));
persona.addAtributo(new Atribute('nombre', new TextType(50), []));
persona.addAtributo(new Atribute('paterno', new TextType(50), []));
persona.addAtributo(new Atribute('materno', new TextType(50), []));
persona.addAtributo(new Atribute('edad', new IntegerType(), []));
persona.addAtributo(new Atribute('sexo_id', new ForeingKeyType('sexo_id', 'sexo', 'id'),[]) )

let paquete = 'com.saul.pruebas';
let modelo = new ModelSpring(persona, paquete);

console.log(modelo.code);